#!/bin/bash
set -e

CI=${CI:-false}

echo "Build starting"

docker build \
    --rm=false --file ./.build/src/Dockerfile.build -t ciceksepeti/%PROJECT_NAME_LOWER%-builder .
docker run ciceksepeti/%PROJECT_NAME_LOWER%-builder > build.tar.gz

if [ "$CI" = true ]; then
    docker build \
        --build-arg container="$CIRCLE_PROJECT_REPONAME" \
        --build-arg imagetag="$CIRCLE_BRANCH-$CIRCLE_BUILD_NUM" \
        --build-arg buildnumber="$CIRCLE_BUILD_NUM" \
        --build-arg commithash="$CIRCLE_SHA1" \
        --build-arg "builddate=$(date)" \
        --rm=false --file ./.build/src/Dockerfile.dist -t ciceksepeti/%PROJECT_NAME_LOWER% .
else
    docker build \
        --rm=false --file ./.build/src/Dockerfile.dist -t ciceksepeti/%PROJECT_NAME_LOWER% .
fi

echo "Build finished"